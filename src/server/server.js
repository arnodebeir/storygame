const express = require('express');
const path = require('path');
const http = require('http');
const PORT = /*process.env.PORT ||*/ 3002;
const socketio = require('socket.io');
const app = express();
const server = http.createServer(app);
const io = socketio(server, {
	cors: {
		origin: '*'
	}
});

app.use(express.static(path.join(__dirname, 'public')));

server.listen(PORT, () => console.log(`Server running on port ${PORT}`));

const connections = [ null, null ];

io.on('connection', (socket) => {
	let playerIndex = -1;
	for (const i in connections) {
		if (connections[i] === null) {
			playerIndex = i;
			break;
		}
	}

	socket.emit('player-number', playerIndex);

	console.log(`Player ${playerIndex} has connected`);

	if (playerIndex === -1) return;

	connections[playerIndex] = false;

	socket.broadcast.emit('player-connection', playerIndex);

	socket.on('disconnect', () => {
		console.log(`Player ${playerIndex} disconnected`);
		connections[playerIndex] = null;
		socket.broadcast.emit('player-connection', playerIndex);
	});

	// On Ready
	socket.on('player-ready', () => {
		socket.broadcast.emit('enemy-ready', playerIndex);
		connections[playerIndex] = true;
	});

	const ready = true;
	for (const i in connections) {
		if (connections[i] === null) {
			ready == false;
		}
	}
	socket.emit('check-players', ready);

	// Check player connections
	socket.on('check-players', () => {
		const players = [];
		for (const i in connections) {
			connections[i] === null
				? players.push({ connected: false, ready: false })
				: players.push({ connected: true, ready: connections[i] });
		}
		socket.emit('check-players', players);
	});
});
