const socket = io();
const playerInfo = document.getElementById('playerInfo');
let playerType = 0;
let ready = false;

socket.on('player-number', (number) => {
	if (number == -1) {
		playerInfo.innerHTML = 'wait for another game';
	} else if (number == 0) {
		playerInfo.innerHTML = 'you are the master';
		playerType = 0;
	} else if (number == 1) {
		playerInfo.innerHTML = 'you are the guesser';
		playerType = 1;
	}
});

socket.on('player-connection', (info) => {
	console.log(info);
});
