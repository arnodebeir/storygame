import Vue from 'vue';
import VueRouter from 'vue-router';
import Game from '../views/Game.vue';
import Home from '../views/Home.vue';
import WaitingRoom from '../views/WaitingRoom.vue';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'Home',
		component: Home
	},
	{
		path: '/waitingRoom',
		name: 'WaitingRoom',
		component: WaitingRoom
	},
	{
		path: '/game',
		name: 'Game',
		component: Game
	}
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
});

export default router;
